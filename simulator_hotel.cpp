#include "simulator_hotel.h"
#include "ui_simulator_hotel.h"
#include <QMessageBox>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include <QTime>

simulator_hotel::simulator_hotel(QWidget *parent) : QMainWindow(parent), ui(new Ui::simulator_hotel)
{
    ui->setupUi(this);
    for (int i = 1; i <= 4; i++)
        data_info[i] = 0;
    data_info[2] = -1;
    data_info[5] = 20;
    data_info[6] = 40;
    ui->lcdNumber->setFrameStyle(QFrame::NoFrame);
    /*Dodanie portow COM do listy*/
    /*Dodanie dostepnych pokoi do listy*/
    Q_FOREACH(QSerialPortInfo port, QSerialPortInfo::availablePorts()) {
            ui->PortName->addItem(port.portName());
    }
    connect(timer, SIGNAL(timeout()), this, SLOT(timer_update()));
    connect(serial, SIGNAL(readyRead()), this, SLOT(leer_serial()));
}

simulator_hotel::~simulator_hotel()
{
    delete ui;
}

void simulator_hotel::timer_update(){
    if (data_info[1] == 1){
        float randomValue = -50 + (float)rand()/(float)(RAND_MAX/(50-(-50)));
        pwr_usage = pwr_usage + (((float)ui->horizontalSlider->value()*3.9) + ((float)ui->horizontalSlider_3->value()*14.1)) + randomValue;
    }
    else
        pwr_usage = pwr_usage + (((float)ui->horizontalSlider->value()*0.99) + ((float)ui->horizontalSlider_3->value()*2.99));
    if(pwr_usage < 1000)
        ui->lb_pwr->setText("<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; color:#5555ff;\">"
                            "" + QString::number(pwr_usage, 'f', 2) + "W</span></p></body></html>");
    else if (pwr_usage >= 1000 && pwr_usage < 1000000)
        ui->lb_pwr->setText("<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; color:#5555ff;\">"
                            "" + QString::number(pwr_usage/1000, 'f', 3) + "kW</span></p></body></html>");
    else if (pwr_usage >= 1000000 && pwr_usage < 1000000000)
        ui->lb_pwr->setText("<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; color:#5555ff;\">"
                            "" + QString::number(pwr_usage/1000000, 'f', 4) + "MW</span></p></body></html>");
    else if (pwr_usage >= 1000000000)
        ui->lb_pwr->setText("<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; color:#5555ff;\">"
                            "" + QString::number(pwr_usage/1000000000, 'f', 5) + "GW</span></p></body></html>");
    if(data_info[7] > 0 ){
        data_info[7]--;
        ui->lcdNumber->display(data_info[7]);
    }
    else if (data_info[7] == 0){
        data_info[3] = 0;
        data_info[7] = -1;
        ui->label_9->setText("<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">CZUJNIK RUCHU - </span>"
                             "<span style=\" font-size:10pt; font-weight:600; color:#1100ff;\">WYŁ</span></p></body></html>");
    }

}

void simulator_hotel::leer_serial()
{
    if (leer_getdata.contains("R3DEND")){
        leer_getdata.clear();
    }
    leer_getdata.append(serial->readAll());
    serial->flush();
    leer_getdata.remove(QRegExp("[\\n\\t\\r]"));
    if(leer_getdata != NULL){
        QStringList splitted_data = leer_getdata.split(";", QString::SkipEmptyParts);

        if (splitted_data.count() > 1){
            if (splitted_data.at(0) == "R3DSCEKKA" && splitted_data.at(1) == "SCAN"){
                if (splitted_data.count() == 4 && splitted_data.at(3) == "R3DEND"){
                        leer_prepareddata = splitted_data.at(2).toInt();
                        if(leer_prepareddata == data_info[2]){
                            data_info[0] = int(pwr_usage);
                            data_info[8] = int((pwr_usage - (int(pwr_usage))) * 100);
                            QString temp1 = "R3DSCEKKA;" + QString::number(data_info[2]) + ";" + QString::number(data_info[4]) + ""
                                            ";" + QString::number(data_info[6]) + ";" + QString::number(data_info[5]) + ""
                                            ";" + QString::number(data_info[3]) + ";" + QString::number(data_info[0]) + ""
                                            ";" + QString::number(data_info[8]) + ";R3DEND";
                            if (serial->write(temp1.toLatin1(), temp1.count()) < 0){
                                ui->statusBar->showMessage("Nie udalo sie wyslac polecenia: '" + temp1 +"'");
                            }
                            else{
                                serial->waitForBytesWritten(50);
                            }
                        }
                }
            }
            else if(splitted_data.at(0) == "R3DSCEKKA" && splitted_data.at(1) == "SET"){
                if (splitted_data.count() == 6 && splitted_data.at(5) == "R3DEND"){
                        leer_prepareddata = splitted_data.at(2).toInt();
                        if(leer_prepareddata == data_info[2]){
                            if(splitted_data.at(3).toInt() == 1){
                                ui->lb_lightset->setText(QString::number(splitted_data.at(4).toInt()) + "%");
                                ui->horizontalSlider->setValue(splitted_data.at(4).toInt());
                                data_info[6] = splitted_data.at(4).toInt();
                            }
                            else if (splitted_data.at(3).toInt() == 2){
                                ui->lb_tempset->setText(QString::number(splitted_data.at(4).toInt()) + "°C");
                                ui->horizontalSlider_3->setValue(splitted_data.at(4).toInt());
                                data_info[5] = splitted_data.at(4).toInt();
                            }
                        }
                }
            }
        }
    }
    if(leer_getdata != NULL)
        ui->statusBar->showMessage("Odebrano: '" + leer_getdata +"'");
}

void simulator_hotel::on_b_port_open_clicked()
{
    serial->setPortName(ui->PortName->currentText());
    serial->setBaudRate(QSerialPort::Baud19200);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);

    if (serial->open(QIODevice::ReadWrite)) {
            ui->statusBar->showMessage("Polaczono z COM" + QString::number(ui->PortName->currentIndex()+1));
            ui->b_port_open->setEnabled(false);
            ui->b_port_close->setEnabled(true);
            ui->simu_start->setEnabled(true);
        } else {
            ui->b_port_open->setEnabled(true);
            ui->b_port_close->setEnabled(false);
            ui->statusBar->showMessage("Błąd: Podany port nie został znaleziony.");
            ui->simu_start->setEnabled(false);
            QMessageBox messageBox;
            messageBox.information(0, "Brak portu", "Nie można odnaleźć określonego portu!");
        }

     serial->clear();
}

void simulator_hotel::on_b_port_close_clicked()
{
    serial->close();
    ui->statusBar->showMessage("Zamknięto połączenie");
    ui->b_port_open->setEnabled(true);
    ui->b_port_close->setEnabled(false);
    ui->simu_start->setEnabled(false);
    timer->stop();
}

void simulator_hotel::on_simu_start_clicked()
{
    ui->label_2->setText("<html><head/><body><p><span style=\" font-size:20pt; font-weight:600;\">Pokój #"+ QString::number(ui->spinBox->value()) +"</span></p></body></html>");
    data_info[2] = ui->spinBox->value();
    timer->start(1000);
}

void simulator_hotel::on_horizontalSlider_3_valueChanged(int value)
{
    ui->lb_tempset->setText(QString::number(value) + "°C");
    data_info[5] = value;
}

void simulator_hotel::on_horizontalSlider_valueChanged(int value)
{
    ui->lb_lightset->setText(QString::number(value) + "%");
    data_info[6] = value;
}

void simulator_hotel::on_pushButton_clicked()
{
    if (data_info[1] == 0){
        data_info[1] = 1;
        ui->label_8->setText("<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">ZUŻYCIE ENERGII "
                             "</span><span style=\" font-size:10pt; font-weight:600; color:#3dcc00;\">+RNG</span></p></body></html>");
    }
    else{
        data_info[1] = 0;
        ui->label_8->setText("<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">ZUŻYCIE ENERGII</span></p></body></html>");
    }
}

void simulator_hotel::on_pb_msens_clicked()
{
    if (data_info[3] == 0){
        data_info[3] = 1;
        data_info[7] = 60;
        ui->label_9->setText("<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">CZUJNIK RUCHU - </span>"
                             "<span style=\" font-size:10pt; font-weight:600; color:#00cece;\">WŁ</span></p></body></html>");
    }
    else{
        data_info[3] = 0;
        data_info[7] = 0;
        ui->label_9->setText("<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">CZUJNIK RUCHU - </span>"
                             "<span style=\" font-size:10pt; font-weight:600; color:#1100ff;\">WYŁ</span></p></body></html>");
    }
}

void simulator_hotel::on_pb_ppozsens_clicked()
{
    if (data_info[4] == 0){
        data_info[4] = 1;
        ui->label_10->setText("<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">CZUJNIK PPOŻ - </span>"
                              "<span style=\" font-size:10pt; font-weight:600; color:#ff0000;\">WŁ</span></p></body></html>");
    }
    else{
        data_info[4] = 0;
        ui->label_10->setText("<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">CZUJNIK PPOŻ - </span>"
                              "<span style=\" font-size:10pt; font-weight:600; color:#03c300;\">WYŁ</span></p></body></html>");
    }
}

void simulator_hotel::on_pushButton_2_clicked()
{
    ui->PortName->clear();
    Q_FOREACH(QSerialPortInfo port, QSerialPortInfo::availablePorts()) {
            ui->PortName->addItem(port.portName());
    }
}

