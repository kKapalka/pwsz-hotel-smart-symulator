#ifndef SIMULATOR_HOTEL_H
#define SIMULATOR_HOTEL_H

#include <QMainWindow>
#include <QSerialPort>
#include <QTimer>

namespace Ui {
class simulator_hotel;
}

class simulator_hotel : public QMainWindow
{
    Q_OBJECT

public:
    explicit simulator_hotel(QWidget *parent = 0);
    ~simulator_hotel();

    QSerialPort *serial = new QSerialPort;
    QTimer *timer = new QTimer;
    float pwr_usage = 0;
    QString leer_getdata;
    QStringList splitted_data;
    int leer_prepareddata;
    int data_info[9];
private slots:
    void leer_serial();
    void on_b_port_open_clicked();
    void on_b_port_close_clicked();
    void on_simu_start_clicked();
    void on_horizontalSlider_3_valueChanged(int value);
    void on_horizontalSlider_valueChanged(int value);
    void on_pushButton_clicked();
    void timer_update();
    void on_pb_msens_clicked();
    void on_pb_ppozsens_clicked();
    void on_pushButton_2_clicked();
private:
    Ui::simulator_hotel *ui;
};

#endif // SIMULATOR_HOTEL_H
